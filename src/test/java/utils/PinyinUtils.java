/**
 * @FileName: PinyinUtils.java
 * @Package: utils
 * @author youshipeng
 * @created 2016/10/27 9:04
 * <p>
 * Copyright 2016 ziroom
 */
package utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
@ContextConfiguration(locations={"classpath:spring/*.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class PinyinUtils {

    @Test
    public void shouldInsertAllWordsIntoPinyinTextTable() throws Exception {
    }
}