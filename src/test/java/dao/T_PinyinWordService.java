package dao;

import dao.elasticsearch.KnowledgeInformationMapper;
import model.PinyinWord;
import model.elasticsearch.KnowledgeInformation;
import model.options.KnowledgeInformationSearchOptions;
import model.value.WordClickCount;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.DisMaxQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import service.PinyinWordService;
import service.elasticsearch.KnowledgeInformationService;
import util.ModelUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.elasticsearch.index.query.QueryBuilders.*;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;
import static util.Utils.UUID;
import static util.Utils.isEmpty;

/**
 * Created by xiaoyou on 16-10-22.
 */
@ContextConfiguration(locations={"classpath:spring/*.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class T_PinyinWordService {

    @Resource(name = "pinyinWordService")
    PinyinWordService pinyinWordService;

    @Resource(name = "knowledgeInformationService")
    KnowledgeInformationService knowledgeInformationService;

    @Resource(name = "knowledgeInformationMapper")
    KnowledgeInformationMapper knowledgeInformationMapper;

    @Test
    public void insertTest() throws Exception {
        PinyinWord pinyinWord = new PinyinWord();
        pinyinWord.setId(UUID());
        pinyinWord.setWord("我们");
        pinyinWord.setAcronym("wm");
        pinyinWord.setWhole("women");
        pinyinWord.setWordLength(2);
        pinyinWord.setAcronymLength(2);
        pinyinWord.setWholeLength(5);
        pinyinWordService.create(pinyinWord);
    }

    @Test
    public void getByIdTest() throws Exception {
        PinyinWord pinyinWord = pinyinWordService.get("85e6835fa2554062adad7c4b9427a2b9");
        ModelUtils.degrade(pinyinWord);
    }

    @Test
    public void analyzeAndSearchTest() throws Exception {
        List<List<WordClickCount>> results = pinyinWordService.analyzeSearch("xiugais机haoqyxgai修改");
        Set<String> content = new HashSet<>();
        StringBuilder sb = new StringBuilder("");
        Integer count = results.size();
        for (List<WordClickCount> son : results) {
            for (WordClickCount clickCount : son) {
                sb.append(clickCount.getWord());
                content.add(clickCount.getWord());
            }
        }

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        DisMaxQueryBuilder disMaxQueryBuilder = disMaxQuery();
        BoolQueryBuilder boolQueryBuilder = boolQuery();

        disMaxQueryBuilder.add(matchQuery("pushContent", sb.toString()));

        boolQueryBuilder.must(termQuery("cityCode", "110000"));

        searchSourceBuilder.query(termsQuery("pushContent", content));
        searchSourceBuilder.postFilter(boolQueryBuilder);
        searchSourceBuilder.highlight(
                new HighlightBuilder().field("pushContent").preTags("@preTag@").postTags("@postTag@")
        );
        searchSourceBuilder.from(0).size(5).sort("_score", SortOrder.DESC).sort("clicks", SortOrder.DESC);

        List<KnowledgeInformation> knowledgeInformation = knowledgeInformationMapper.search(searchSourceBuilder);

//        long start1 = System.currentTimeMillis();
//        for (int i = 0; i < 100; i++) {
//            long start = System.currentTimeMillis();
//            List<List<WordClickCount>> results1 = pinyinWordService.analyzeSearch("xiugais机haoqyxgai修改");
//            long end = System.currentTimeMillis();
//            System.out.println(end - start + " ms");
//        }
//        long end1 = System.currentTimeMillis();
//        System.out.println(end1 - start1 + " ms");
//        Assert.assertNotNull(results);
    }
}
