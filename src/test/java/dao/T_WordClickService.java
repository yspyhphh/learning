package dao;

import model.WordClick;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import service.WordClickService;

import javax.annotation.Resource;

import static util.Utils.UUID;

/**
 * Created by xiaoyou on 16-10-22.
 */
@ContextConfiguration(locations={"classpath:spring/*.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class T_WordClickService {

    @Resource(name = "wordClickService")
    WordClickService wordClickService;

    @Test
    public void shouldInsertOneWordClick() throws Exception {
        WordClick wordClick = new WordClick();
        wordClick.setId(UUID());
        wordClick.setWordContent("床前明月光");
//        wordClickService.create(wordClick);
        wordClickService.update("c8069199125b4aa1bc2dddffffd75ab5", wordClick);
    }
}
