package dao;

import model.PinyinWord;
import model.elasticsearch.KnowledgeInformation;
import model.elasticsearch.Tokenizer;
import model.options.KnowledgeInformationSearchOptions;
import model.options.PinyinWordSearchOptions;
import org.elasticsearch.index.query.QueryBuilders;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import service.PinyinWordService;
import service.elasticsearch.KnowledgeInformationService;
import util.Utils;

import javax.annotation.Resource;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;
import static util.PinyinUtils.converterToSpell;
import static util.Utils.isCollectionEmpty;
import static util.Utils.isNull;

/**
 * Created by xiaoyou on 16-10-22.
 */
@ContextConfiguration(locations={"classpath:spring/*.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class T_KnowledgeInformation {

    @Resource(name = "knowledgeInformationService")
    KnowledgeInformationService knowledgeInformationService;

    @Resource(name = "pinyinWordService")
    PinyinWordService pinyinWordService;

    @Test
    public void tokenizerTest() {
//        converterToSpell("归给").split("[$]")[0].replace("u:", "v");
        List<String> results = knowledgeInformationService.analyze("我们都有一个家", Tokenizer.HAN_INDEX);
        String a = null;
        System.out.println(new StringBuilder("").append(a).toString());
    }

    @Test
    public void syncData() throws Exception {
        KnowledgeInformationSearchOptions options = new KnowledgeInformationSearchOptions();
        options.setPageSize(5000);
        List<KnowledgeInformation> informations = knowledgeInformationService.search(options);
        System.out.println(informations);
    }

    @Test
    public void syncPinyinText() throws Exception {
        KnowledgeInformationSearchOptions searchOptions = new KnowledgeInformationSearchOptions();
        searchOptions.setPageSize(100000);
        searchOptions.setPageNumber(0);
        List<KnowledgeInformation> results = knowledgeInformationService.search(searchOptions);
        for (KnowledgeInformation information : results) {
            StringBuilder source = new StringBuilder("").append(isNull(information.getPushContent(), ""))
                    .append(isNull(information.getClientContent(), "")).append(isNull(information.getSimilarQuestions(), ""));
            List<String> words = knowledgeInformationService.analyze(source.toString(), Tokenizer.IK_MAX_WORD);
            for (String word : words) {
                if (word.length() < 2) {
                    continue;
                }
                try {
                    Integer.parseInt(word);
                    continue;
                } catch (Exception e) {

                }
                System.out.println("--------------------- " + word + " start ------------------------------");
                PinyinWordSearchOptions options = new PinyinWordSearchOptions();
                options.setWord(word);
                List<PinyinWord> pinyinWords = pinyinWordService.search(options);
                if (isCollectionEmpty(pinyinWords)) {
                    String[] pinyinResults = converterToSpell(word).split("[$]");
                    for (int i = 0; i < pinyinResults.length; i++) {
                        String[] result = pinyinResults[i].split(",");
                        String whole = "";
                        String acronym = "";
                        for (int j = 0; j < result.length; j++) {
                            String repair = result[j].replace("u:", "v");
                            whole += repair;
                            acronym += repair.charAt(0);
                        }
                        PinyinWord pinyinWord1 = new PinyinWord();
                        pinyinWord1.setId(Utils.UUID());
                        pinyinWord1.setWord(word);
                        pinyinWord1.setAcronym(acronym);
                        pinyinWord1.setWhole(whole);
                        pinyinWord1.setWordLength(word.length());
                        pinyinWord1.setAcronymLength(acronym.length());
                        pinyinWord1.setWholeLength(whole.length());
                        pinyinWordService.create(pinyinWord1);
                        System.out.println(pinyinWord1.getAcronym() + ", " +pinyinWord1.getWhole() + ", " +pinyinWord1.getWord());
                    }
                }
                System.out.println("--------------------- " + word + " end ------------------------------");
            }
        }
    }

}
