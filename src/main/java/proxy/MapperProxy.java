package proxy;

import org.elasticsearch.action.ActionFuture;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthRequest;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.cluster.health.ClusterHealthStatus;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

/**
 * Created by xiaoyou on 8/17/16.
 */
public class MapperProxy {

    private ElasticsearchTemplate elasticsearchTemplate;

    private MapperProxy() {

    }

    public void setElasticsearchTemplate(ElasticsearchTemplate elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    public void clusterHealth() {
        try {
            ActionFuture<ClusterHealthResponse> health =
                    elasticsearchTemplate.getClient().admin().cluster().health(new ClusterHealthRequest());
            ClusterHealthStatus status = health.actionGet().getStatus();
            if (status.value() == ClusterHealthStatus.RED.value()) {
                throw new RuntimeException("elasticsearch cluster health status is red.");
            }
        } catch (Exception e) {
//            logger.error("ping elasticsearch error.", e);
        }
    }
}
