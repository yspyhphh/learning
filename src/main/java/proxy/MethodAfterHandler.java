/**
 * @FileName: ExceptionHandler.java
 * @Package: com.ziroom.zmc.services.knowledge.proxy
 * @author youshipeng
 * @created 2016/10/31 9:24
 * <p>
 * Copyright 2016 ziroom
 */
package proxy;

import org.springframework.aop.AfterReturningAdvice;

import java.lang.reflect.Method;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public class MethodAfterHandler implements AfterReturningAdvice {
    @Override
    public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
        System.out.println();
        System.out.println();
    }
}