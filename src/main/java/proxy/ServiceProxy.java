package proxy;

import exception.BusinessException;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Random;

@Component
@Aspect
public class ServiceProxy {

    private final static Logger LOGGER = Logger.getLogger(ServiceProxy.class);

    @Around("execution(* service..*.*(..))")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        Object[] args = point.getArgs(); // 参数
        LOGGER.info("request begin args[" + Arrays.toString(args) + "].");
        Object returnValue = null;
        try {
            returnValue = point.proceed(args);
        } catch (BusinessException e) {
            LOGGER.info("catch business exception.");
            throw e;
        } catch (Exception e) {
            throw e;
        }
        return returnValue;
    }

    @Before("execution(* service..*.*(..))")
    public void before(JoinPoint point) throws Throwable {
        LOGGER.info("congratulation!");
    }

    @AfterReturning(pointcut = "execution(* service..*.*(..))", returning="returnValue")
    public void afterReturning(JoinPoint point, Object returnValue) throws Throwable {
        LOGGER.info("return value[" + returnValue + "].");
    }

    @After("execution(* service..*.*(..))")
    public void after(JoinPoint point) throws Throwable {
        LOGGER.info("Bey bey!");
    }

}