/**
 * @FileName: ExceptionHandler.java
 * @Package: com.ziroom.zmc.services.knowledge.proxy
 * @author youshipeng
 * @created 2016/10/31 9:24
 * <p>
 * Copyright 2016 ziroom
 */
package proxy;

import org.springframework.aop.ThrowsAdvice;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public class ExceptionHandler implements ThrowsAdvice {
    public void afterThrowing(Throwable throwable) throws Throwable {
        System.out.println();
        System.out.println();
    }

    public static void main(String[] args) {
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(1477560587954l)));
    }
}