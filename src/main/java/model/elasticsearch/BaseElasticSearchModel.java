package model.elasticsearch;

import model.BaseModel;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Map;

/**
 * Created by xiaoyou on 7/7/16.
 */
public class BaseElasticSearchModel extends BaseModel {

    protected float score;
    protected Map<String, String> highlightFields;

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public Map<String, String> getHighlightFields() {
        return highlightFields;
    }

    public void setHighlightFields(Map<String, String> highlightFields) {
        this.highlightFields = highlightFields;
    }
}
