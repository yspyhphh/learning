/**
 * @FileName: KnowledgeElasticsearchEntity.java
 * @Package: com.ziroom.zmc.services.knowledge.entity.elasticsearch
 * @author youshipeng
 * @created 2016/9/9 15:22
 * <p>
 * Copyright 2016 ziroom
 */
package model.elasticsearch;

import model.elasticsearch.BaseElasticSearchModel;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
@Document(indexName = "knowledge_repository_d", type = "knowledge_information")
public class KnowledgeInformation extends BaseElasticSearchModel {
    @Field(type = FieldType.String, index = FieldIndex.not_analyzed)
    private String knowledgeId;

    @Field(type = FieldType.String, index = FieldIndex.not_analyzed)
    private String firstItemId;

    @Field(type = FieldType.String, index = FieldIndex.not_analyzed)
    private String firstItemName;

    @Field(type = FieldType.String, index = FieldIndex.not_analyzed)
    private String secondItemId;

    @Field(type = FieldType.String, index = FieldIndex.not_analyzed)
    private String secondItemName;

    @Field(type = FieldType.String, index = FieldIndex.not_analyzed)
    private String thirdItemId;

    @Field(type = FieldType.String, index = FieldIndex.not_analyzed)
    private String thirdItemName;

    @Field(type = FieldType.String, index = FieldIndex.not_analyzed)
    private String fourthItemId;

    @Field(type = FieldType.String, index = FieldIndex.not_analyzed)
    private String fourthItemName;

    @Field(type = FieldType.String, index = FieldIndex.analyzed, searchAnalyzer = "han_search", analyzer = "han_index")
    private String name;

    @Field(type = FieldType.String, index = FieldIndex.analyzed, searchAnalyzer = "han_search", analyzer = "han_index")
    private String pushContent;

    @Field(type = FieldType.String, index = FieldIndex.analyzed, searchAnalyzer = "han_search", analyzer = "han_index")
    private String clientContent;

    @Field(type = FieldType.String, index = FieldIndex.analyzed, searchAnalyzer = "han_search", analyzer = "han_index")
    private String similarQuestions;

    @Field(type = FieldType.Boolean, index = FieldIndex.not_analyzed)
    private boolean toUser;

    @Field(type = FieldType.Integer, index = FieldIndex.not_analyzed)
    private Integer clicks;

    @Field(type = FieldType.String, index = FieldIndex.not_analyzed)
    private String cityCode;

    @Field(type = FieldType.String, index = FieldIndex.not_analyzed)
    private String solve;

    @Field(type = FieldType.String, index = FieldIndex.not_analyzed)
    private String remark;

    @Field(type = FieldType.String, index = FieldIndex.not_analyzed)
    private String drawling;

    public String getDrawling() {
        return drawling;
    }

    public void setDrawling(String drawling) {
        this.drawling = drawling;
    }

    public String getSolve() {
        return solve;
    }

    public void setSolve(String solve) {
        this.solve = solve;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public Integer getClicks() {
        return clicks;
    }

    public void setClicks(Integer clicks) {
        this.clicks = clicks;
    }

    public boolean isToUser() {
        return toUser;
    }

    public void setToUser(boolean toUser) {
        this.toUser = toUser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKnowledgeId() {
        return knowledgeId;
    }

    public String getFirstItemName() {
        return firstItemName;
    }

    public void setFirstItemName(String firstItemName) {
        this.firstItemName = firstItemName;
    }

    public String getSecondItemName() {
        return secondItemName;
    }

    public void setSecondItemName(String secondItemName) {
        this.secondItemName = secondItemName;
    }

    public String getThirdItemName() {
        return thirdItemName;
    }

    public void setThirdItemName(String thirdItemName) {
        this.thirdItemName = thirdItemName;
    }

    public String getFourthItemName() {
        return fourthItemName;
    }

    public void setFourthItemName(String fourthItemName) {
        this.fourthItemName = fourthItemName;
    }

    public void setKnowledgeId(String knowledgeId) {
        this.knowledgeId = knowledgeId;
    }

    public String getFirstItemId() {
        return firstItemId;
    }

    public void setFirstItemId(String firstItemId) {
        this.firstItemId = firstItemId;
    }

    public String getSecondItemId() {
        return secondItemId;
    }

    public void setSecondItemId(String secondItemId) {
        this.secondItemId = secondItemId;
    }

    public String getThirdItemId() {
        return thirdItemId;
    }

    public void setThirdItemId(String thirdItemId) {
        this.thirdItemId = thirdItemId;
    }

    public String getFourthItemId() {
        return fourthItemId;
    }

    public void setFourthItemId(String fourthItemId) {
        this.fourthItemId = fourthItemId;
    }

    public String getPushContent() {
        return pushContent;
    }

    public void setPushContent(String pushContent) {
        this.pushContent = pushContent;
    }

    public String getClientContent() {
        return clientContent;
    }

    public void setClientContent(String clientContent) {
        this.clientContent = clientContent;
    }

    public String getSimilarQuestions() {
        return similarQuestions;
    }

    public void setSimilarQuestions(String similarQuestions) {
        this.similarQuestions = similarQuestions;
    }


}