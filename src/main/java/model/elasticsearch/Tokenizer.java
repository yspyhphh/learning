/**
 * @FileName: AnalyzeConstants.java
 * @Package: model.elasticsearch
 * @author youshipeng
 * @created 2016/11/7 10:11
 * <p>
 * Copyright 2016 ziroom
 */
package model.elasticsearch;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public enum Tokenizer {
    IK_MAX_WORD("ik_max_word"),
    IK_SMART("ik_smart"),
    IK("ik"),
    HAN_INDEX("han_index"),
    HAN_SEARCH("han_search");

    private String title;

    public String getTitle() {
        return title;
    }

    Tokenizer(String title) {
        this.title = title;
    }
}