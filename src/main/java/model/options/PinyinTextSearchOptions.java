/**
 * @FileName: PinyinSearchOptions.java
 * @Package: model.options
 * @author youshipeng
 * @created 2016/11/1 8:50
 * <p>
 * Copyright 2016 ziroom
 */
package model.options;

import model.options.page.Pageable;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public class PinyinTextSearchOptions extends Pageable {
    private String pinyin;
    private String chineseFilter;
    private String contentText;

    public String getContentText() {
        return contentText;
    }

    public void setContentText(String contentText) {
        this.contentText = contentText;
    }

    public String getChineseFilter() {
        return chineseFilter;
    }

    public void setChineseFilter(String chineseFilter) {
        this.chineseFilter = chineseFilter;
    }

    public String getPinyin() {
        return pinyin;
    }

    public void setPinyin(String pinyin) {
        this.pinyin = pinyin;
    }
}