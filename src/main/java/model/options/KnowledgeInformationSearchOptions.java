/**
 * @FileName: KnowledgeInformationSearchOptions.java
 * @Package: com.ziroom.zmc.services.knowledge.entity.options
 * @author youshipeng
 * @created 2016/9/13 19:17
 * <p>
 * Copyright 2016 ziroom
 */
package model.options;

import model.options.page.Pageable;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public class KnowledgeInformationSearchOptions extends Pageable {
    private String question;
    private String cityCode;
    private String fourthItemId;
    private boolean toUser = false;
    private boolean hotspot = false;

    public String getFourthItemId() {
        return fourthItemId;
    }

    public void setFourthItemId(String fourthItemId) {
        this.fourthItemId = fourthItemId;
    }

    public boolean isHotspot() {
        return hotspot;
    }

    public void setHotspot(boolean hotspot) {
        this.hotspot = hotspot;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public boolean isToUser() {
        return toUser;
    }

    public void setToUser(boolean toUser) {
        this.toUser = toUser;
    }
}