/**
 * @FileName: Pageable.java
 * @Package: model.options.page
 * @author youshipeng
 * @created 2016/11/1 8:50
 * <p>
 * Copyright 2016 ziroom
 */
package model.options.page;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public class Pageable {
    private boolean paging = true;
    private Integer pageNumber = 1;
    private Integer pageSize = 10;
    private Integer startCount;
    private Integer endCount;

    public boolean isPaging() {
        return paging;
    }

    public void setPaging(boolean paging) {
        this.paging = paging;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getStartCount() {
        return (pageNumber - 1) * pageSize;
    }

    public Integer getEndCount() {
        return pageNumber * pageSize;
    }
}