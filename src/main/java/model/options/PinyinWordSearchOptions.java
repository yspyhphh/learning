/**
 * @FileName: PinyinSearchOptions.java
 * @Package: model.options
 * @author youshipeng
 * @created 2016/11/1 8:50
 * <p>
 * Copyright 2016 ziroom
 */
package model.options;

import model.options.page.Pageable;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public class PinyinWordSearchOptions extends Pageable {
    private String word;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}