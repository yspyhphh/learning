/**
 * @FileName: PinyinSearchOptions.java
 * @Package: model.options
 * @author youshipeng
 * @created 2016/11/1 8:50
 * <p>
 * Copyright 2016 ziroom
 */
package model.options;

import model.options.page.Pageable;
import util.enums.LexemeType;
import util.enums.SentenceType;

import static util.Utils.isEmpty;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public class PinyinWordAnalyzeSearchOptions extends Pageable {
    private String wholeSearch;
    private String chineseSearch;
    private String acronymSearch;
    private String chineseFilter;
    private String pinyinFilter;
    private LexemeType lexemeType;

    public PinyinWordAnalyzeSearchOptions(String chineseSearch, String wholeSearch, String acronymSearch,
                                          String chineseFilter, String pinyinFilter, LexemeType lexemeType) {
        this.wholeSearch = wholeSearch;
        this.chineseSearch = chineseSearch;
        this.acronymSearch = acronymSearch;
        this.chineseFilter = chineseFilter;
        this.pinyinFilter = pinyinFilter;
        this.lexemeType = lexemeType;
    }

    public LexemeType getLexemeType() {
        return lexemeType;
    }

    public void setLexemeType(LexemeType lexemeType) {
        this.lexemeType = lexemeType;
    }

    public String getWholeSearch() {
        return wholeSearch;
    }

    public void setWholeSearch(String wholeSearch) {
        this.wholeSearch = wholeSearch;
    }

    public String getChineseSearch() {
        return chineseSearch;
    }

    public void setChineseSearch(String chineseSearch) {
        this.chineseSearch = chineseSearch;
    }

    public String getAcronymSearch() {
        return acronymSearch;
    }

    public void setAcronymSearch(String acronymSearch) {
        this.acronymSearch = acronymSearch;
    }

    public String getChineseFilter() {
        if (isEmpty(chineseFilter) || "%".equals(chineseFilter)) {
            return null;
        }
        return chineseFilter;
    }

    public void setChineseFilter(String chineseFilter) {
        this.chineseFilter = chineseFilter;
    }

    public String getPinyinFilter() {
        if (isEmpty(pinyinFilter) || "%".equals(pinyinFilter)) {
            return null;
        }
        return pinyinFilter;
    }

    public void setPinyinFilter(String pinyinFilter) {
        this.pinyinFilter = pinyinFilter;
    }
}