/**
 * @FileName: PinyinText.java
 * @Package: model
 * @author youshipeng
 * @created 2016/10/27 8:58
 * <p>
 * Copyright 2016 ziroom
 */
package model;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public class WordClick extends BaseModel {
    private String wordContent;

    public String getWordContent() {
        return wordContent;
    }

    public void setWordContent(String wordContent) {
        this.wordContent = wordContent;
    }
}