/**
 * @FileName: PinyinWord.java
 * @Package: model
 * @author youshipeng
 * @created 2016/11/7 13:04
 * <p>
 * Copyright 2016 ziroom
 */
package model;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public class PinyinWord extends BaseModel {
    private String word;
    private String whole;
    private String acronym;
    private Integer wordLength;
    private Integer wholeLength;
    private Integer acronymLength;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getWhole() {
        return whole;
    }

    public void setWhole(String whole) {
        this.whole = whole;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public Integer getWordLength() {
        return wordLength;
    }

    public void setWordLength(Integer wordLength) {
        this.wordLength = wordLength;
    }

    public Integer getWholeLength() {
        return wholeLength;
    }

    public void setWholeLength(Integer wholeLength) {
        this.wholeLength = wholeLength;
    }

    public Integer getAcronymLength() {
        return acronymLength;
    }

    public void setAcronymLength(Integer acronymLength) {
        this.acronymLength = acronymLength;
    }
}