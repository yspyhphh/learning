package model;

/**
 * Created by xiaoyou on 16-7-4.
 */
public class BaseModel {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
