/**
 * @FileName: WordClickCount.java
 * @Package: model.value
 * @author youshipeng
 * @created 2016/11/2 9:02
 * <p>
 * Copyright 2016 ziroom
 */
package model.value;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public class WordClickCount {
    private String word;
    private Integer clickCount;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Integer getClickCount() {
        return clickCount;
    }

    public void setClickCount(Integer clickCount) {
        this.clickCount = clickCount;
    }
}