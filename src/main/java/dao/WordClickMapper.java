/**
 * @FileName: FoodMapper.java
 * @Package: dao
 * @author youshipeng
 * @created 2016/10/25 19:11
 * <p>
 * Copyright 2016 ziroom
 */
package dao;

import model.WordClick;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public interface WordClickMapper extends BaseMapper<WordClick> {
}