package dao;

import dao.provider.PublicProvider;
import model.BaseModel;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;

import java.util.List;

/**
 * Created by xiaoyou on 16-7-7.
 */
public interface BaseMapper<T extends BaseModel> {

    @InsertProvider(type = PublicProvider.class, method = "insert")
    void insert(T entity);

    List<T> getAll() throws Exception;

    T get(String id);

    @UpdateProvider(type = PublicProvider.class, method = "update")
    void update(@Param(value = "id") String id, @Param(value = "entity") T entity);

    void delete(String id);

    void bulkInsert(List<T> entities);

    void bulkDelete(List<String> ids);

}

