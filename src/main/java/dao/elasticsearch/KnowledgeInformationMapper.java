/**
 * @FileName: KnowledgeInformationMapper.java
 * @Package: com.ziroom.zmc.services.knowledge.dao.elasticsearch
 * @author youshipeng
 * @created 2016/9/10 9:42
 * <p>
 * Copyright 2016 ziroom
 */
package dao.elasticsearch;

import model.elasticsearch.KnowledgeInformation;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public class KnowledgeInformationMapper extends BaseElasticSearchMapper<KnowledgeInformation> {
}