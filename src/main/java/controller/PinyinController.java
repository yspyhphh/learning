/**
 * @FileName: PinyinController.java
 * @Package: controller
 * @author youshipeng
 * @created 2016/11/1 8:39
 * <p>
 * Copyright 2016 ziroom
 */
package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
@Controller
@RequestMapping("/pinyin")
public class PinyinController {
}