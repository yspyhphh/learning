/**
 * @FileName: ChineseSentence.java
 * @Package: util.model
 * @author youshipeng
 * @created 2016/11/6 14:39
 * <p>
 * Copyright 2016 ziroom
 */
package util.model;

import util.enums.LexemeType;
import util.enums.SentenceType;

import java.util.List;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public class ChineseSentence {
    private String content;
    private List<Lexeme> sentenceUnits;
    private SentenceType sentenceType;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Lexeme> getSentenceUnits() {
        return sentenceUnits;
    }

    public SentenceType getSentenceType() {
        return sentenceType;
    }

    public void setSentenceUnits(List<Lexeme> sentenceUnits) {
        this.sentenceUnits = sentenceUnits;
        initSentenceType();
    }

    private void initSentenceType() {
        sentenceType = SentenceType.CHINESE_SENTENCE;
        for (Lexeme lexeme : sentenceUnits) {
            if (lexeme.getLexemeType() == LexemeType.ACRONYM) {
                sentenceType = SentenceType.ACRONYM_SENTENCE;
                break;
            } else if (lexeme.getLexemeType() == LexemeType.WHOLE
                    && sentenceType == SentenceType.CHINESE_SENTENCE) {
                sentenceType = SentenceType.WHOLE_SENTENCE;
            }
        }
    }
}