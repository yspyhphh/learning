/**
 * @FileName: Lexeme.java
 * @Package: util.model
 * @author youshipeng
 * @created 2016/11/6 14:23
 * <p>
 * Copyright 2016 ziroom
 */
package util.model;

import util.enums.LexemeType;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public class Lexeme {
    private String content;
    private LexemeType lexemeType;

    public Lexeme(String content, LexemeType lexemeType) {
        this.lexemeType = lexemeType;
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LexemeType getLexemeType() {
        return lexemeType;
    }

    public void setLexemeType(LexemeType lexemeType) {
        this.lexemeType = lexemeType;
    }
}