package util;

/**
 * Created by xiaoyou on 7/7/16.
 */
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Constants.
 */
public final class Constants {
    public static final Integer DEFAULT_OWNER_ID = 0;

    public static final String SYSTEM_INTERNAL = "SystemInternal";

    public static final Integer DEFAULT_BLACKLIST_DAYS = 21;

    public static final Integer SIGNIN_BLACKLIST_DAYS = 1;

    public static final Integer OFFER_BLACKLIST_DAYS = 8;

    public static final Integer ONBOARD_BLACKLIST_DAYS = 45;

    public static final Integer RESUME_BLACKLIST_DAYS = 180;

    public static final String LINE_SEPARATOR = System.getProperty("line.separator");

    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    public static final DateFormat DATETIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public static final DateFormat TIMESTAMP_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static final DateFormat TIME_RANGE_FORMAT = new SimpleDateFormat("HH:mm");

    public static final DateFormat CALLRECORD_TIMESTAMP_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");

    public static final DateFormat TIME_MSEC_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");


    private Constants() {
        //private ctor
    }
}
