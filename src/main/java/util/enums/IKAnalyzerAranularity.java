package util.enums;

/**
 * Created by xiaoyou on 8/16/16.
 */
public enum  IKAnalyzerAranularity {
    IGH_PRECISION,
    LOW_PRECISION;
}
