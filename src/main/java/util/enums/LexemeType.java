/**
 * @FileName: LexemeType.java
 * @Package: util.model
 * @author youshipeng
 * @created 2016/11/6 14:24
 * <p>
 * Copyright 2016 ziroom
 */
package util.enums;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public enum LexemeType {
    CHINESE(3000), WHOLE(2000), ACRONYM(1000);

    private Integer weight;

    LexemeType(Integer weight) {
        this.weight = weight;
    }

    public static LexemeType changeUp(LexemeType type1, LexemeType type2) {
        if (type1.weight > type2.weight) {
            return type1;
        }
        return type2;
    }

    public static LexemeType changeDown(LexemeType type1, LexemeType type2) {
        if (type1 == null) return type2;
        if (type2 == null) return type1;
        if (type1.weight < type2.weight) {
            return type1;
        }
        return type2;
    }
}