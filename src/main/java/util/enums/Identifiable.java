package util.enums;

import java.io.Serializable;

/**
 * Created by youshipeng on 2016/8/4.
 */
public interface Identifiable<T> extends Serializable {
    T getId();
}
