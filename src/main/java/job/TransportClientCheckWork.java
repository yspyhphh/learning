package job;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.cluster.node.DiscoveryNode;
import org.elasticsearch.common.transport.TransportAddress;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

import java.util.List;

/**
 * Created by xiaoyou on 8/20/16.
 */
public class TransportClientCheckWork {
    private ElasticsearchTemplate elasticsearchTemplate;

    private TransportClient client;

    public void setElasticsearchTemplate(ElasticsearchTemplate elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
        this.client = (TransportClient) this.elasticsearchTemplate.getClient();
    }


    public void execute() {
        List<TransportAddress> addresses = client.transportAddresses();
        List<DiscoveryNode> connectedNodes = client.connectedNodes();
//        if (!addresses.containsAll(new ArrayList<Object>(){{
//            add(new InetSocketTransportAddress(new InetSocketAddress("127.0.0.1", 9300)));
//            add(new InetSocketTransportAddress(new InetSocketAddress("127.0.0.1", 9301)));
//        }})) {
//            client.addTransportAddress(new InetSocketTransportAddress(new InetSocketAddress("127.0.0.1", 9300)));
//            client.addTransportAddress(new InetSocketTransportAddress(new InetSocketAddress("127.0.0.1", 9301)));
//        }
    }

}
