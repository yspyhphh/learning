/**
 * @FileName: RedisService.java
 * @Package: service.redis
 * @author youshipeng
 * @created 2016/11/11 13:00
 * <p>
 * Copyright 2016 ziroom
 */
package service.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
@Service(value = "redisService")
public class RedisService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    public String get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    public Set<String> sss(String pattern) {
        return redisTemplate.keys("*2*2*");
    }

    public void set(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
    }

    public void set(String key, Integer value) {
        redisTemplate.opsForValue().set(key, String.valueOf(value));
    }
}