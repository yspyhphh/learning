/**
 * @FileName: PinyinTextService.java
 * @Package: service
 * @author youshipeng
 * @created 2016/10/27 20:18
 * <p>
 * Copyright 2016 ziroom
 */
package service;

import model.WordClick;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public interface WordClickService extends BaseService<WordClick> {
}
