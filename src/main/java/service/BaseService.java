/**
 * @FileName: BaseService.java
 * @Package: service
 * @author youshipeng
 * @created 2016/11/1 9:07
 * <p>
 * Copyright 2016 ziroom
 */
package service;

import model.BaseModel;
import service.redis.RedisService;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public interface BaseService<MODEL extends BaseModel> {

    RedisService redis();

    MODEL create(MODEL model);
//
//    MODEL createOrUpdate(MODEL model);
//
    MODEL get(String id);

    MODEL update(String id, MODEL model);
//
//    void delete(Integer id);
//
//    void softDelete(Integer id);
//
//    boolean exists(Integer id);
//
//    List<MODEL> getAll();
//
//    Results<MODEL> getAllByPagination(PageableOptions options);
//
//    List<MODEL> batchGet(List<Integer> ids);
//
//    List<AutoSuggestResult> getAutoSuggestNames();
}