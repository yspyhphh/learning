/**
 * @FileName: PinyinTextServiceImpl.java
 * @Package: service.impl
 * @author youshipeng
 * @created 2016/10/27 20:19
 * <p>
 * Copyright 2016 ziroom
 */
package service.impl;

import dao.WordClickMapper;
import model.WordClick;
import org.springframework.stereotype.Service;
import service.WordClickService;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
@Service(value = "wordClickService")
public class WordClickServiceImpl extends BaseServiceImpl<WordClick, WordClickMapper> implements WordClickService {
}