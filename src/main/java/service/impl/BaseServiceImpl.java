/**
 * @FileName: BaseServiceImpl.java
 * @Package: service.impl
 * @author youshipeng
 * @created 2016/11/1 9:11
 * <p>
 * Copyright 2016 ziroom
 */
package service.impl;

import dao.BaseMapper;
import model.BaseModel;
import org.springframework.beans.factory.annotation.Autowired;
import service.BaseService;
import service.redis.RedisService;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public class BaseServiceImpl<MODEL extends BaseModel, MAPPER extends BaseMapper<MODEL>> implements BaseService<MODEL> {

    @Autowired
    protected MAPPER mapper;

    @Autowired
    private RedisService redis;

    @Override
    public RedisService redis() {
        return redis;
    }

    @Override
    public MODEL get(String id) {
        return mapper.get(id);
    }

    @Override
    public MODEL create(MODEL model) {
        mapper.insert(model);
        return model;
    }

    @Override
    public MODEL update(String id, MODEL model) {
        mapper.update(id, model);
        return model;
    }
}