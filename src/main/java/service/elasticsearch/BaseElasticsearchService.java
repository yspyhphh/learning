/**
 * @FileName: BaseService.java
 * @Package: service
 * @author youshipeng
 * @created 2016/11/1 9:07
 * <p>
 * Copyright 2016 ziroom
 */
package service.elasticsearch;

import model.elasticsearch.BaseElasticSearchModel;
import model.elasticsearch.Tokenizer;
import model.options.page.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import service.BaseService;

import java.util.List;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public interface BaseElasticsearchService<MODEL extends BaseElasticSearchModel> extends BaseService<MODEL> {
    List<String> analyze(String source, Tokenizer tokenizer);
}