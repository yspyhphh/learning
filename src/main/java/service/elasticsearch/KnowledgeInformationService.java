/**
 * @FileName: KnowledgeInformationService.java
 * @Package: service.elasticsearch
 * @author youshipeng
 * @created 2016/11/7 10:30
 * <p>
 * Copyright 2016 ziroom
 */
package service.elasticsearch;

import model.elasticsearch.KnowledgeInformation;
import model.options.KnowledgeInformationSearchOptions;

import java.util.List;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public interface KnowledgeInformationService extends BaseElasticsearchService<KnowledgeInformation> {

    List<KnowledgeInformation> search(KnowledgeInformationSearchOptions options) throws Exception;
}