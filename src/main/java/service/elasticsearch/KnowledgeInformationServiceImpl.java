/**
 * @FileName: KnowledgeInformationServiceImpl.java
 * @Package: service.elasticsearch
 * @author youshipeng
 * @created 2016/11/7 10:31
 * <p>
 * Copyright 2016 ziroom
 */
package service.elasticsearch;

import dao.elasticsearch.KnowledgeInformationMapper;
import model.elasticsearch.KnowledgeInformation;
import model.options.KnowledgeInformationSearchOptions;
import model.options.page.Pageable;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.DisMaxQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.*;
import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static util.Utils.isEmpty;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
@Service(value = "knowledgeInformationService")
public class KnowledgeInformationServiceImpl extends BaseElasticsearchServiceImpl<KnowledgeInformation, KnowledgeInformationMapper>
        implements KnowledgeInformationService {

    @Override
    public List<KnowledgeInformation> search(KnowledgeInformationSearchOptions options) throws Exception {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        DisMaxQueryBuilder disMaxQueryBuilder = disMaxQuery();
        BoolQueryBuilder boolQueryBuilder = boolQuery();

        String question = options.getQuestion();
        if (!isEmpty(question)) {
            disMaxQueryBuilder.add(matchQuery("pushContent", question));
//            if (options.isToUser()) {
//                disMaxQueryBuilder.add(matchQuery("similarQuestions", question)).add(matchQuery("clientContent", question))
//                        .add(matchQuery("pushContent", question));
//                boolQueryBuilder.must(termQuery("toUser", true));
//            } else {
//                disMaxQueryBuilder.add(matchQuery("name", question)).add(matchQuery("similarQuestions", question));
//            }
//            disMaxQueryBuilder.tieBreaker(0.3f);
        } else {
            disMaxQueryBuilder.add(matchAllQuery());
        }

        if (!isEmpty(options.getFourthItemId())) {
            boolQueryBuilder.must(termQuery("fourthItemId", options.getFourthItemId()));
        }

        if (!isEmpty(options.getCityCode())) {
            boolQueryBuilder.must(termQuery("cityCode", options.getCityCode()));
        }

        searchSourceBuilder.query(disMaxQueryBuilder);
        searchSourceBuilder.postFilter(boolQueryBuilder);
        searchSourceBuilder.highlight(
                new HighlightBuilder().field("pushContent")
                        .preTags("@preTag@").postTags("@postTag@")
        );
        searchSourceBuilder.from(options.getStartCount()).size(options.getPageSize())
                .sort("_score", SortOrder.DESC);

        if (options.isHotspot()) {
            searchSourceBuilder.sort(new FieldSortBuilder("clicks").order(SortOrder.DESC));
        }

        List<KnowledgeInformation> knowledgeInformation = mapper.search(searchSourceBuilder);
        return knowledgeInformation;
    }
}