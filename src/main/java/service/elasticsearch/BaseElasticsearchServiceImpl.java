/**
 * @FileName: BaseElasticsearchServiceImpl.java
 * @Package: service.elasticsearch
 * @author youshipeng
 * @created 2016/11/7 10:26
 * <p>
 * Copyright 2016 ziroom
 */
package service.elasticsearch;

import dao.elasticsearch.BaseElasticSearchMapper;
import model.elasticsearch.BaseElasticSearchModel;
import model.elasticsearch.KnowledgeInformation;
import model.elasticsearch.Tokenizer;
import model.options.page.Pageable;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.DisMaxQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import service.redis.RedisService;

import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.*;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public class BaseElasticsearchServiceImpl<MODEL extends BaseElasticSearchModel, MAPPER extends BaseElasticSearchMapper<MODEL>>
        implements BaseElasticsearchService<MODEL> {

    @Autowired
    protected MAPPER mapper;

    @Autowired
    private RedisService redis;

    @Override
    public RedisService redis() {
        return redis;
    }

    @Override
    public MODEL create(MODEL model) {
        mapper.insert(model);
        return model;
    }

    @Override
    public MODEL update(String id, MODEL model) {
        mapper.update(id, model);
        return model;
    }

    @Override
    public MODEL get(String id) {
        return mapper.get(id);
    }

    @Override
    public List<String> analyze(String source, Tokenizer tokenizer) {
        return mapper.analyze(source, tokenizer);
    }
}