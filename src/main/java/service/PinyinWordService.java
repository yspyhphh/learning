/**
 * @FileName: PinyinTextService.java
 * @Package: service
 * @author youshipeng
 * @created 2016/10/27 20:18
 * <p>
 * Copyright 2016 ziroom
 */
package service;

import model.PinyinWord;
import model.options.PinyinWordAnalyzeSearchOptions;
import model.options.PinyinWordSearchOptions;
import model.value.WordClickCount;

import java.util.List;

/**
 * <p></p>
 *
 * <PRE>
 * <BR>	修改记录
 * <BR>-----------------------------------------------
 * <BR>	修改日期			修改人			修改内容
 * </PRE>
 *
 * @author youshipeng
 * @since 1.0
 * @version 1.0
 */
public interface PinyinWordService extends BaseService<PinyinWord> {
    List<List<WordClickCount>> analyzeSearch(String input) throws Exception;

    List<WordClickCount> searchByClickCount(PinyinWordAnalyzeSearchOptions options);

    List<PinyinWord> search(PinyinWordSearchOptions options);
}
