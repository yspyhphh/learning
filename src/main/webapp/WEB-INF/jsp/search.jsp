<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%
    String path = request.getContextPath();
%>
<html>
<head>
    <title>Title</title>
    <script src="${path}/js/jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            initEvent();
        });

        function initEvent() {
            $("#input").on("change keyup", function() {
                $("#time").html("");
                var start = new Date().getTime();
                reload();
                var end = new Date().getTime();
                $("#time").html("耗时： " + (end - start) + " ms");
            });
        }

        function reload() {
            var input = $("#input").val();
            if (!input) {
                return false;
            }
            $.ajax({
                url: "${path}/elasticsearch/analyze",
                data: JSON.stringify({"input": input}),
                contentType: "application/json",
                dataType: "json",
                async: false,
                type: "POST",
                success: function (data) {
                    var values = data.data;
                    $("#result").html("");
                    for (var value in values) {
                        $("#result").append(values[value] + "</br>");
                    }
                },
                error: function (msg) {
                    alert(msg);
                }
            });
        }
    </script>
</head>
<body>
        <input type="text" name="input" id="input" />

        <div id="result">

        </div>

        <div id="time">

        </div>
</body>
</html>
